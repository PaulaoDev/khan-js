<p align="center">
    <img  src="https://i.imgur.com/IcRnxr3.png "  alt="KhanJS"  width="200px"/>
</p>

<p align="center">
    <a href="https://scrutinizer-ci.com/g/PaulaoDev/khan-framework/?branch=master"><img src="https://scrutinizer-ci.com/g/PaulaoDev/khan-framework/badges/quality-score.png?b=master" alt="Passing"></a>
    <a href="https://travis-ci.org/PaulaoDev/khan-framework"><img src="https://travis-ci.org/PaulaoDev/khan-framework.svg?branch=master"></a>
</p>
<p align="center">
    <a href="https://github.com/PaulaoDev/khan-framework/fork"><img src="https://img.shields.io/github/forks/PaulaoDev/khan-framework.svg"></a>
    <a href="https://raw.githubusercontent.com/PaulaoDev/khan-framework/master/LICENSE"><img src="https://img.shields.io/badge/license-MIT-blue.svg"></a>
    <a href="https://github.com/PaulaoDev/khan-framework/issues"><img src="https://img.shields.io/github/issues/PaulaoDev/khan-framework.svg"></a>
</p>


## Installation

```html
<!-- CDN -->
<script src="https://cdn.rawgit.com/PaulaoDev/khan-framework/master/dist/index.js"></script>
<!-- OR OUTHER CDN -->
<script src="https://rawgit.com/PaulaoDev/khan-framework/master/dist/index.js"></script>
<!-- OR STATIC FILE -->
<script src="dist/index.js"></script>
```

## Demo
 - index.html

## License

MIT © [PaulaoDev](jskhanframework@gmail.com)
